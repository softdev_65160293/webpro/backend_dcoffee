export class CreateCheckDto {
  checkItems: {
    materialId: number;
    qty: string;
  }[];
  userId: number;
}
