import { Module } from '@nestjs/common';
import { CheckService } from './check.service';
import { CheckController } from './check.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Check } from './entities/check.entity';
import { CheckItem } from './entities/checkItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Material } from 'src/material/entities/material.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Check, CheckItem, User, Material])],
  controllers: [CheckController],
  providers: [CheckService],
})
export class CheckModule {}
