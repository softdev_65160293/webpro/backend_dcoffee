import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Check } from './check.entity';
import { Material } from 'src/material/entities/material.entity';

@Entity()
export class CheckItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  unit: number;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @ManyToOne(() => Check, (check) => check.checkItems, { onDelete: 'CASCADE' })
  check: Check;

  @ManyToOne(() => Material, (material) => material.checkItems)
  material: Material;
}
