import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CheckItem } from './checkItem.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Check {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  total: number;

  @Column()
  qty: number;

  @CreateDateColumn()
  created: Date;

  @OneToMany(() => CheckItem, (checkItem) => checkItem.check)
  checkItems: CheckItem[];

  @ManyToOne(() => User, (user) => user.checks)
  user: User;
}
