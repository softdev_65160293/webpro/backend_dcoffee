import { Injectable } from '@nestjs/common';
import { CreateCheckDto } from './dto/create-check.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Check } from './entities/check.entity';
import { Repository } from 'typeorm';
import { CheckItem } from './entities/checkItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Material } from 'src/material/entities/material.entity';

@Injectable()
export class CheckService {
  constructor(
    @InjectRepository(Check) private checkRepository: Repository<Check>,
    @InjectRepository(CheckItem)
    private checkItemsRepository: Repository<CheckItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  async create(createCheckDto: CreateCheckDto) {
    const check = new Check();
    const user = await this.usersRepository.findOneBy({
      id: createCheckDto.userId,
    });
    check.user = user;
    check.total = 0;
    check.qty = 0;
    check.checkItems = [];
    for (const oi of createCheckDto.checkItems) {
      const checkItem = new CheckItem();
      checkItem.material = await this.materialsRepository.findOneBy({
        id: oi.materialId,
      });
      checkItem.name = checkItem.material.name;
      checkItem.price = checkItem.material.price;
      checkItem.unit = parseInt(oi.qty);
      checkItem.total = checkItem.price * checkItem.unit;
      await this.checkItemsRepository.save(checkItem);
      check.checkItems.push(checkItem);
      check.total += checkItem.total;
      check.qty += checkItem.unit;
    }
    return this.checkRepository.save(check);
  }

  findAll() {
    return this.checkRepository.find({
      relations: {
        checkItems: true,
        user: true,
      },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.checkRepository.findOneOrFail({
      where: { id },
      relations: { checkItems: true },
    });
  }

  async remove(id: number) {
    const deleteCheck = await this.checkRepository.findOneOrFail({
      where: { id },
    });
    await this.checkRepository.remove(deleteCheck);

    return deleteCheck;
  }
}
