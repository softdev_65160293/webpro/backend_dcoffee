export class CreateOrderDto {
  orderItems: {
    materialId: number;
    qty: string;
  }[];
  userId: number;
}
