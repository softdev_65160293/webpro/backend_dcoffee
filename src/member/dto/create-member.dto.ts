export class CreateMemberDto {
  id: number;
  email: string;
  username: string;
  password: string;
  name: string;
  tel: string;
  point: number;
  image: string;
  gender: string;
}
