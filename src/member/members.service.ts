import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    console.log(createMemberDto.image);
    const member = new Member();
    member.email = createMemberDto.email;
    member.gender = createMemberDto.gender;
    member.username = createMemberDto.username;
    member.password = createMemberDto.password;
    member.name = createMemberDto.name;
    member.tel = createMemberDto.tel;
    member.point = createMemberDto.point;
    if (createMemberDto.image && createMemberDto.image !== '') {
      member.image = createMemberDto.image;
    }
    console.log(createMemberDto.image);
    return this.membersRepository.save(member);
  }

  findAll() {
    return this.membersRepository.find();
  }

  findOne(id: number) {
    return this.membersRepository.findOne({
      where: { id },
    });
  }

  findOneByTel(tel: string) {
    return this.membersRepository.findOneOrFail({
      where: { tel },
    });
  }

  findOneByUsername(username: string) {
    return this.membersRepository.findOneOrFail({
      where: { username },
    });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    const member = new Member();
    member.email = updateMemberDto.email;
    member.gender = updateMemberDto.gender;
    member.username = updateMemberDto.username;
    member.password = updateMemberDto.password;
    member.name = updateMemberDto.name;
    member.tel = updateMemberDto.tel;
    member.point = updateMemberDto.point;
    if (updateMemberDto.image && updateMemberDto.image !== '') {
      member.image = updateMemberDto.image;
    }

    const updateMember = await this.membersRepository.findOneOrFail({
      where: { id },
    });

    updateMemberDto.email = member.email;
    updateMemberDto.gender = member.gender;
    updateMemberDto.username = member.username;
    updateMemberDto.password = member.password;
    updateMemberDto.name = member.name;
    updateMemberDto.tel = member.tel;
    updateMemberDto.point = member.point;
    this.membersRepository.save(updateMember);

    const result = await this.membersRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteMember = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    await this.membersRepository.remove(deleteMember);

    return deleteMember;
  }
}
