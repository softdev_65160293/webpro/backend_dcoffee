import { Receipt } from 'src/receipt/entities/receipt.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    default: 'member@mail.com',
  })
  email: string;

  @Column({
    default: 'user',
  })
  username: string;

  @Column({
    default: 'password',
  })
  password: string;

  @Column({
    default: 'none',
  })
  name: string;

  @Column({
    default: 'Male',
  })
  gender: string;

  @Column({
    default: '00000000',
  })
  tel: string;

  @Column({
    default: 0,
  })
  point: number;

  @Column({ default: 'noimg.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Receipt, (receipt) => receipt.member)
  receipt: Receipt[];
}
