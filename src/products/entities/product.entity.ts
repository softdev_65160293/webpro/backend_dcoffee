import { ReceiptItems } from 'src/receipt/entities/receiptItems.entity';
import { TypeProduct } from 'src/types_product/entities/typeProduct.entity';

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => TypeProduct, (type) => type.products)
  type: TypeProduct;

  @OneToMany(() => ReceiptItems, (orderItem) => orderItem.product)
  orderItems: ReceiptItems[];
}
