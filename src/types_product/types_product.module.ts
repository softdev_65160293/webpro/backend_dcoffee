import { Module } from '@nestjs/common';
import { Types_ProductService } from './types_product.service';
import { Types_ProductController } from './types_product.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeProduct } from './entities/typeProduct.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TypeProduct])],
  controllers: [Types_ProductController],
  providers: [Types_ProductService],
})
export class Types_ProductModule {}
