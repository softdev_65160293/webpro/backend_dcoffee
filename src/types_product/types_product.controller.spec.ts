import { Test, TestingModule } from '@nestjs/testing';
import { Types_ProductController } from './types_product.controller';
import { Types_ProductService } from './types_product.service';

describe('TypesController', () => {
  let controller: Types_ProductController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Types_ProductController],
      providers: [Types_ProductService],
    }).compile();

    controller = module.get<Types_ProductController>(Types_ProductController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
