import { Injectable } from '@nestjs/common';
import { CreateTypeDto } from './dto/create-type.dto';
import { UpdateTypeDto } from './dto/update-type.dto';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeProduct } from './entities/typeProduct.entity';
@Injectable()
export class Types_ProductService {
  constructor(
    @InjectRepository(TypeProduct)
    private typesRepository: Repository<TypeProduct>,
  ) {}
  create(createTypeDto: CreateTypeDto) {
    return this.typesRepository.save(createTypeDto);
  }

  findAll() {
    return this.typesRepository.find({ relations: { products: true } });
  }

  findOne(id: number) {
    return this.typesRepository.findOneBy({ id });
  }

  async update(id: number, updateTypeDto: UpdateTypeDto) {
    await this.typesRepository.findOneByOrFail({ id });
    await this.typesRepository.update(id, updateTypeDto);
    const updatedType = await this.typesRepository.findOneBy({ id });
    return updatedType;
  }

  async remove(id: number) {
    const removedType = await this.typesRepository.findOneByOrFail({ id });
    await this.typesRepository.remove(removedType);
    return removedType;
  }
}
