import { Test, TestingModule } from '@nestjs/testing';
import { Bills_PlaceService } from './bills_place.service';

describe('Bills_PlaceService', () => {
  let service: Bills_PlaceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Bills_PlaceService],
    }).compile();

    service = module.get<Bills_PlaceService>(Bills_PlaceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
