import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Bills_Place {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    default: 'Place',
  })
  type: string;

  @Column()
  totalPrice: number;

  @Column()
  date: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
