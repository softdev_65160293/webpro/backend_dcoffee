import { Test, TestingModule } from '@nestjs/testing';
import { Bills_PlaceController } from './bills_place.controller';
import { Bills_PlaceService } from './bills_place.service';

describe('Bills_PlaceController', () => {
  let controller: Bills_PlaceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Bills_PlaceController],
      providers: [Bills_PlaceService],
    }).compile();

    controller = module.get<Bills_PlaceController>(Bills_PlaceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
