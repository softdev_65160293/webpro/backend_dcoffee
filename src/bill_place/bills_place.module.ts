import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bills_PlaceController } from './bills_place.controller';
import { Bills_PlaceService } from './bills_place.service';
import { Bills_Place } from './entities/bills_place.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bills_Place])],
  controllers: [Bills_PlaceController],
  providers: [Bills_PlaceService],
})
export class Bills_PlaceModule {}
