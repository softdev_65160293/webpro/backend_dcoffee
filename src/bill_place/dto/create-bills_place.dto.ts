export class CreateBills_PlaceDto {
  type: string;

  totalPrice: number;

  date: string;
}
