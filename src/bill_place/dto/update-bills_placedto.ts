import { PartialType } from '@nestjs/mapped-types';
import { CreateBills_PlaceDto } from './create-bills_place.dto';

export class UpdateBills_PlaceDto extends PartialType(CreateBills_PlaceDto) {}
