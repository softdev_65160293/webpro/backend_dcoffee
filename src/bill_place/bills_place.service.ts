import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Bills_Place } from './entities/bills_place.entity';
import { CreateBills_PlaceDto } from './dto/create-bills_place.dto';
import { UpdateBills_PlaceDto } from './dto/update-bills_placedto';

@Injectable()
export class Bills_PlaceService {
  constructor(
    @InjectRepository(Bills_Place)
    private bills_placeRepository: Repository<Bills_Place>,
  ) {}

  create(createBills_PlaceDto: CreateBills_PlaceDto) {
    return this.bills_placeRepository.save(createBills_PlaceDto);
  }

  findAll(): Promise<Bills_Place[]> {
    return this.bills_placeRepository.find({});
  }

  findOne(id: number) {
    return this.bills_placeRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateBills_PlaceDto: UpdateBills_PlaceDto) {
    await this.bills_placeRepository.findOneByOrFail({ id });
    const beforeUpdate = await this.bills_placeRepository.findOneBy({ id });
    await this.bills_placeRepository.update(id, updateBills_PlaceDto);
    const afterUpdate = await this.bills_placeRepository.findOneBy({ id });
    return { beforeUpdate, afterUpdate };
  }

  async remove(id: number) {
    const billRemove = await this.bills_placeRepository.findOneByOrFail({
      id,
    });
    await this.bills_placeRepository.remove(billRemove);
    return billRemove;
  }
}
