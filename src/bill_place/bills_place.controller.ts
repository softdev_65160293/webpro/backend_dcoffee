import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { Bills_PlaceService } from './bills_place.service';
import { UpdateBills_PlaceDto } from './dto/update-bills_placedto';
import { CreateBills_PlaceDto } from './dto/create-bills_place.dto';

@Controller('bills_place')
export class Bills_PlaceController {
  constructor(private readonly bills_placeService: Bills_PlaceService) {}

  @Post()
  create(@Body() createBills_PlaceDto: CreateBills_PlaceDto) {
    return this.bills_placeService.create(createBills_PlaceDto);
  }

  @Get()
  findAll() {
    return this.bills_placeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bills_placeService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBills_PlaceDto: UpdateBills_PlaceDto,
  ) {
    return this.bills_placeService.update(+id, updateBills_PlaceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bills_placeService.remove(+id);
  }
}
