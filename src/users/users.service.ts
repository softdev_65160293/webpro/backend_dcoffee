import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  create(createUserDto: CreateUserDto) {
    console.log(createUserDto.image);
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.gender = createUserDto.gender;
    user.username = createUserDto.username;
    user.password = createUserDto.password;
    user.role = createUserDto.role;
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    console.log(createUserDto.image);
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find();
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      where: { email },
    });
  }

  findOneByUsername(username: string) {
    return this.usersRepository.findOneOrFail({
      where: { username },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.gender = updateUserDto.gender;
    user.username = updateUserDto.username;
    user.password = updateUserDto.password;
    user.role = updateUserDto.role;
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }

    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });

    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;
    updateUser.role = user.role;
    this.usersRepository.save(updateUser);

    const result = await this.usersRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteUser);

    return deleteUser;
  }
}
// import { Injectable } from '@nestjs/common';
// import { CreateUserDto } from './dto/create-user.dto';
// import { UpdateUserDto } from './dto/update-user.dto';
// import { Repository } from 'typeorm';
// import { User } from './entities/user.entity';
// import { InjectRepository } from '@nestjs/typeorm';
// import { Role } from 'src/roles/entities/role.entity';

// @Injectable()
// export class UsersService {
//   findAll() {
//     return this.usersRepository.find();
//   }
//   async findOne(id: number): Promise<User> {
//     return await this.usersRepository.findOne({ where: { id } });
//   }

//   constructor(
//     @InjectRepository(User) private usersRepository: Repository<User>,
//     @InjectRepository(Role) private rolesRepository: Repository<Role>,
//   ) {}

//   async create(createUserDto: CreateUserDto): Promise<User> {
//     const user = new User();
//     Object.assign(user, createUserDto);
//     if (createUserDto.image && createUserDto.image !== '') {
//       user.image = createUserDto.image;
//     }
//     return await this.usersRepository.save(user);
//   }

//   async findAdminsAndUsers(): Promise<User[]> {
//     return await this.usersRepository.find({
//       where: [{ role: 'admin' }, { role: 'user' }],
//     });
//   }

//   async findOneById(id: number): Promise<User> {
//     return await this.usersRepository.findOne({ where: { id } });
//   }
//   async findOneByEmail(email: string): Promise<User> {
//     return await this.usersRepository.findOne({
//       where: { email },
//     });
//   }

//   async update(id: number, updateUserDto: UpdateUserDto): Promise<User> {
//     const user = await this.usersRepository.findOneOrFail({ where: { id } });
//     Object.assign(user, updateUserDto);
//     if (updateUserDto.image && updateUserDto.image !== '') {
//       user.image = updateUserDto.image;
//     }
//     const updatedUser = await this.usersRepository.save(user);
//     return updatedUser;
//   }
//   async remove(id: number): Promise<User> {
//     const user = await this.usersRepository.findOneOrFail({ where: { id } });
//     await this.usersRepository.remove(user);
//     return user;
//   }
// }
