import { Check } from 'src/stock/checkMat/entities/check.entity';
import { Order } from 'src/stock/orderMat/entities/order.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column({
    default: 'user',
  })
  username: string;

  @Column()
  password: string;

  @Column({
    name: 'full-Name',
    default: '',
  })
  fullName: string;

  @Column({
    name: 'role',
    default: 'Admin',
  })
  role: string;

  @Column()
  gender: string;

  @Column({ default: 'noimg.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Check, (check) => check.user)
  checks: Check[];

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];
}

// import {
//   Entity,
//   PrimaryGeneratedColumn,
//   Column,
//   CreateDateColumn,
//   UpdateDateColumn,
// } from 'typeorm';

// @Entity()
// export class User {
//   @PrimaryGeneratedColumn()
//   id: number;

//   @Column()
//   email: string;

//   @Column()
//   username: string;

//   @Column()
//   password: string;

//   @Column({
//     name: 'full-Name',
//     default: '',
//   })
//   fullName: string;

//   @Column({
//     name: 'role',
//     default: 'Admin',
//   })
//   role: string;

//   @Column()
//   gender: string;

//   @Column({ default: 'noimage.jpg' })
//   image: string;

//   @CreateDateColumn()
//   created: Date;

//   @UpdateDateColumn()
//   updated: Date;
//   checks: any;
//   orders: any;
// }
