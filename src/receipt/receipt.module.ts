import { Module } from '@nestjs/common';
import { ReceiptsService } from './receipt.service';
import { ReceiptsController } from './receipt.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { ReceiptItems } from './entities/receiptItems.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
import { Member } from 'src/member/entities/member.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Receipt, ReceiptItems, User, Product, Member]),
  ],
  controllers: [ReceiptsController],
  providers: [ReceiptsService],
})
export class ReceiptsModule {}
