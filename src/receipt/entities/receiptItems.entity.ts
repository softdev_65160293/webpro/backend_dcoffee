import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Receipt } from './receipt.entity';
import { Product } from 'src/products/entities/product.entity';

@Entity()
export class ReceiptItems {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Receipt, (order) => order.orderItems, {
    onDelete: 'CASCADE',
  })
  order: Receipt;

  @ManyToOne(() => Product, (product) => product.orderItems)
  product: Product;
}
