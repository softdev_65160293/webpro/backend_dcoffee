import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ReceiptItems } from './receiptItems.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/member/entities/member.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  total: number;

  @Column()
  qty: number;

  @Column()
  paymentType: string;

  @Column()
  discount: number;

  @Column()
  receicedAmount: number;

  @Column()
  change: number;

  @Column()
  netTotal: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => ReceiptItems, (orderItem) => orderItem.order)
  orderItems: ReceiptItems[];

  @ManyToOne(() => User, (user) => user.orders)
  user: User;

  @ManyToOne(() => Member, (member) => member.receipt)
  member: Member;
}
