import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { ReceiptItems } from './entities/receiptItems.entity';
import { User } from 'src/users/entities/user.entity';
import { Product } from 'src/products/entities/product.entity';
import { Member } from 'src/member/entities/member.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt) private receiptsRepository: Repository<Receipt>,
    @InjectRepository(ReceiptItems)
    private orderItemsRepository: Repository<ReceiptItems>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Member) private membersRepository: Repository<Member>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto) {
    const order = new Receipt();
    const user = await this.usersRepository.findOneBy({
      id: createReceiptDto.userId,
    });
    const member = await this.membersRepository.findOneBy({
      id: createReceiptDto.memberId,
    });
    order.user = user;
    order.member = member;
    order.total = 0;
    order.qty = 0;
    order.orderItems = [];
    order.discount = createReceiptDto.discount;
    order.paymentType = createReceiptDto.paymentType;
    for (const oi of createReceiptDto.orderItems) {
      const orderItem = new ReceiptItems();
      orderItem.product = await this.productsRepository.findOneBy({
        id: oi.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.qty = oi.qty;
      orderItem.total = orderItem.price * orderItem.qty;
      await this.orderItemsRepository.save(orderItem);
      order.orderItems.push(orderItem);
      order.total += orderItem.total;
      order.qty += orderItem.qty;
    }
    order.netTotal = order.total - order.discount;
    if (order.paymentType == 'scan') {
      order.change = 0;
      order.receicedAmount = 0;
    } else {
      order.change = createReceiptDto.receicedAmount - order.netTotal;
      order.receicedAmount = createReceiptDto.receicedAmount;
    }
    return this.receiptsRepository.save(order);
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: {
        orderItems: true,
        user: true,
        member: true,
      },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.receiptsRepository.findOneOrFail({
      where: { id },
      relations: { orderItems: true, user: true, member: true },
    });
  }

  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return updateReceiptDto;
  }

  async remove(id: number) {
    const deleteReceipt = await this.receiptsRepository.findOneOrFail({
      where: { id },
    });
    await this.receiptsRepository.remove(deleteReceipt);

    return deleteReceipt;
  }
}
