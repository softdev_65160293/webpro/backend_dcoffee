export class CreateReceiptDto {
  orderItems: {
    productId: number;
    qty: number;
  }[];
  userId: number;
  paymentType: string;
  discount: number;
  receicedAmount: number;
  memberId: number;
}
