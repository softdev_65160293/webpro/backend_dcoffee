import { Injectable } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-user.dto';
import { UpdateMaterialDto } from './dto/update-user.dto';
import { Material } from './entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}

  create(createMaterialDto: CreateMaterialDto) {
    return this.materialRepository.save(createMaterialDto);
  }

  findAll(): Promise<Material[]> {
    return this.materialRepository.find({ relations: { type: true } });
  }

  findOne(id: number) {
    return this.materialRepository.findOne({
      where: { id },
      relations: { type: true },
    });
  }

  findAllByType(typeId: number) {
    return this.materialRepository.find({
      where: { type: { id: typeId } },
      relations: { type: true },
      order: { name: 'ASC' },
    });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    return this.materialRepository.update(id, updateMaterialDto);
  }

  async remove(id: number) {
    const deleteMaterial = await this.materialRepository.findOneOrFail({
      where: { id },
    });
    return await this.materialRepository.remove(deleteMaterial);
  }
}
