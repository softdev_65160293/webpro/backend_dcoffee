import { CheckItem } from 'src/stock/checkMat/entities/checkItem.entity';
import { OrderItem } from 'src/stock/orderMat/entities/orderItem.entity';
import { Type } from 'src/types/entities/typeMaterial.entity';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  unit: number;

  @Column()
  minimum: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Type, (type) => type.materials)
  type: Type;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.material)
  orderItems: OrderItem[];

  @OneToMany(() => CheckItem, (checkItem) => checkItem.material)
  checkItems: CheckItem[];
}
