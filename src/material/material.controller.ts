import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { MaterialService } from './material.service';
import { CreateMaterialDto } from './dto/create-user.dto';
import { UpdateMaterialDto } from './dto/update-user.dto';

@Controller('materials')
export class MaterialController {
  constructor(private readonly materialsService: MaterialService) {}
  // Create
  @Post()
  create(@Body() createMaterialDto: CreateMaterialDto) {
    return this.materialsService.create(createMaterialDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.materialsService.findAll();
  }

  @Get('type/:typeId')
  findAllByType(@Param('typeId') typeId: number) {
    return this.materialsService.findAllByType(typeId);
  }

  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.materialsService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateMaterialDto: UpdateMaterialDto,
  ) {
    return this.materialsService.update(+id, updateMaterialDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.materialsService.remove(+id);
  }
}
