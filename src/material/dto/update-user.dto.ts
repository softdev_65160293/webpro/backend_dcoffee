import { PartialType } from '@nestjs/swagger';
import { CreateMaterialDto } from './create-user.dto';

export class UpdateMaterialDto extends PartialType(CreateMaterialDto) {}
