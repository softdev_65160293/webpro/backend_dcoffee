export class CreateMaterialDto {
  name: string;

  price: number;

  unit: number;

  minimum: number;

  types: string;
}
