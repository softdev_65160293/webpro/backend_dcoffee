import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Material } from './material/entities/material.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { MaterialModule } from './material/material.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { DataSource } from 'typeorm';
import { Type } from './types/entities/typeMaterial.entity';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { Product } from './products/entities/product.entity';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { OrdersModule } from './stock/orderMat/orders.module';
import { CheckModule } from './stock/checkMat/check.module';
import { Check } from './stock/checkMat/entities/check.entity';
import { CheckItem } from './stock/checkMat/entities/checkItem.entity';
import { Order } from './stock/orderMat/entities/order.entity';
import { OrderItem } from './stock/orderMat/entities/orderItem.entity';
import { Receipt } from './receipt/entities/receipt.entity';
import { ReceiptItems } from './receipt/entities/receiptItems.entity';
import { Member } from './member/entities/member.entity';
import { MembersModule } from './member/members.module';
import { Bills_ElectricModule } from './bill_electric/bills_electric.module';
import { Bills_Electric } from './bill_electric/entities/bills_electric.entity';
import { Bills_WaterModule } from './bill_water/bills_water.module';
import { Bills_Water } from './bill_water/entities/bills_water.entity';
import { Bills_Place } from './bill_place/entities/bills_place.entity';
import { Bills_PlaceModule } from './bill_place/bills_place.module';
import { TypeProduct } from './types_product/entities/typeProduct.entity';
import { Types_ProductModule } from './types_product/types_product.module';
import { ReceiptsModule } from './receipt/receipt.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        Material,
        Type,
        Product,
        User,
        Bills_Place,
        Bills_Water,
        Bills_Electric,
        TypeProduct,
        Check,
        CheckItem,
        Order,
        OrderItem,
        Receipt,
        ReceiptItems,
        Member,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    MaterialModule,
    TypesModule,
    ProductsModule,
    UsersModule,
    AuthModule,
    Bills_PlaceModule,
    Bills_WaterModule,
    Bills_ElectricModule,
    OrdersModule,
    CheckModule,
    ReceiptsModule,
    MembersModule,
    Types_ProductModule,
    MembersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
