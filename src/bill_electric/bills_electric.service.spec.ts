import { Test, TestingModule } from '@nestjs/testing';
import { Bills_ElectricService } from './bills_electric.service';

describe('Bills_ElectricService', () => {
  let service: Bills_ElectricService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Bills_ElectricService],
    }).compile();

    service = module.get<Bills_ElectricService>(Bills_ElectricService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
