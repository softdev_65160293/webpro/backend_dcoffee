import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Bills_Electric } from './entities/bills_electric.entity';
import { CreateBills_ElectricDto } from './dto/create-bills_electric.dto';
import { UpdateBills_ElectricDto } from './dto/update-bills_electric.dto';

@Injectable()
export class Bills_ElectricService {
  constructor(
    @InjectRepository(Bills_Electric)
    private bills_electricRepository: Repository<Bills_Electric>,
  ) {}

  create(createBills_ElectricDto: CreateBills_ElectricDto) {
    return this.bills_electricRepository.save(createBills_ElectricDto);
  }

  findAll(): Promise<Bills_Electric[]> {
    return this.bills_electricRepository.find({});
  }

  findOne(id: number) {
    return this.bills_electricRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateBills_ElectricDto: UpdateBills_ElectricDto) {
    await this.bills_electricRepository.findOneByOrFail({ id });
    const beforeUpdate = await this.bills_electricRepository.findOneBy({ id });
    await this.bills_electricRepository.update(id, updateBills_ElectricDto);
    const afterUpdate = await this.bills_electricRepository.findOneBy({ id });
    return { beforeUpdate, afterUpdate };
  }

  async remove(id: number) {
    const billRemove = await this.bills_electricRepository.findOneByOrFail({
      id,
    });
    await this.bills_electricRepository.remove(billRemove);
    return billRemove;
  }
}
