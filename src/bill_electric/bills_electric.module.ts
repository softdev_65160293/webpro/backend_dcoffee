import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bills_ElectricController } from './bills_electric.controller';
import { Bills_ElectricService } from './bills_electric.service';
import { Bills_Electric } from './entities/bills_electric.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bills_Electric])],
  controllers: [Bills_ElectricController],
  providers: [Bills_ElectricService],
})
export class Bills_ElectricModule {}
