import { Test, TestingModule } from '@nestjs/testing';
import { Bills_ElectricController } from './bills_electric.controller';
import { Bills_ElectricService } from './bills_electric.service';

describe('Bills_ElectricController', () => {
  let controller: Bills_ElectricController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Bills_ElectricController],
      providers: [Bills_ElectricService],
    }).compile();

    controller = module.get<Bills_ElectricController>(Bills_ElectricController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
