import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { Bills_ElectricService } from './bills_electric.service';
import { UpdateBills_ElectricDto } from './dto/update-bills_electric.dto';
import { CreateBills_ElectricDto } from './dto/create-bills_electric.dto';

@Controller('bills_electric')
export class Bills_ElectricController {
  constructor(private readonly bills_electricService: Bills_ElectricService) {}

  @Post()
  create(@Body() createBills_ElectricDto: CreateBills_ElectricDto) {
    return this.bills_electricService.create(createBills_ElectricDto);
  }

  @Get()
  findAll() {
    return this.bills_electricService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bills_electricService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBills_ElectricDto: UpdateBills_ElectricDto,
  ) {
    return this.bills_electricService.update(+id, updateBills_ElectricDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bills_electricService.remove(+id);
  }
}
