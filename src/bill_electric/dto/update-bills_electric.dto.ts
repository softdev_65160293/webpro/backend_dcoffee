import { PartialType } from '@nestjs/mapped-types';
import { CreateBills_ElectricDto } from './create-bills_electric.dto';

export class UpdateBills_ElectricDto extends PartialType(
  CreateBills_ElectricDto,
) {}
