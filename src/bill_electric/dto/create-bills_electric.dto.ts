export class CreateBills_ElectricDto {
  type: string;

  usedUnit: number;

  unitToBath: number;

  totalPrice: number;

  date: string;
}
