import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { Bills_WaterService } from './bills_waterservice';
import { UpdateBills_WaterDto } from './dto/update-bills_water.dto';
import { CreateBills_WaterDto } from './dto/create-bills_water.dto';

@Controller('bills_water')
export class Bills_WaterController {
  constructor(private readonly bills_waterService: Bills_WaterService) {}

  @Post()
  create(@Body() createBills_WaterDto: CreateBills_WaterDto) {
    return this.bills_waterService.create(createBills_WaterDto);
  }

  @Get()
  findAll() {
    return this.bills_waterService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bills_waterService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBills_WaterDto: UpdateBills_WaterDto,
  ) {
    return this.bills_waterService.update(+id, updateBills_WaterDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bills_waterService.remove(+id);
  }
}
