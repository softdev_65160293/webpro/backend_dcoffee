import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Bills_Water } from './entities/bills_water.entity';
import { CreateBills_WaterDto } from './dto/create-bills_water.dto';
import { UpdateBills_WaterDto } from './dto/update-bills_water.dto';

@Injectable()
export class Bills_WaterService {
  constructor(
    @InjectRepository(Bills_Water)
    private bills_waterRepository: Repository<Bills_Water>,
  ) {}

  create(createBills_WaterDto: CreateBills_WaterDto) {
    return this.bills_waterRepository.save(createBills_WaterDto);
  }

  findAll(): Promise<Bills_Water[]> {
    return this.bills_waterRepository.find({});
  }

  findOne(id: number) {
    return this.bills_waterRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateBills_WaterDto: UpdateBills_WaterDto) {
    await this.bills_waterRepository.findOneByOrFail({ id });
    const beforeUpdate = await this.bills_waterRepository.findOneBy({ id });
    await this.bills_waterRepository.update(id, updateBills_WaterDto);
    const afterUpdate = await this.bills_waterRepository.findOneBy({ id });
    return { beforeUpdate, afterUpdate };
  }

  async remove(id: number) {
    const billRemove = await this.bills_waterRepository.findOneByOrFail({
      id,
    });
    await this.bills_waterRepository.remove(billRemove);
    return billRemove;
  }
}
