import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Bills_Water {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    default: 'Water',
  })
  type: string;

  @Column()
  usedUnit: number;

  @Column()
  unitToBath: number;

  @Column()
  totalPrice: number;

  @Column()
  date: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
