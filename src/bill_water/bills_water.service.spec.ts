import { Test, TestingModule } from '@nestjs/testing';
import { Bills_WaterService } from './bills_waterservice';

describe('Bills_WaterService', () => {
  let service: Bills_WaterService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Bills_WaterService],
    }).compile();

    service = module.get<Bills_WaterService>(Bills_WaterService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
