import { PartialType } from '@nestjs/mapped-types';
import { CreateBills_WaterDto } from './create-bills_water.dto';

export class UpdateBills_WaterDto extends PartialType(CreateBills_WaterDto) {}
