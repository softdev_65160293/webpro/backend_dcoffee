export class CreateBills_WaterDto {
  type: string;

  usedUnit: number;

  unitToBath: number;

  totalPrice: number;

  date: string;
}
