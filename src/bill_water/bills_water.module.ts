import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bills_WaterController } from './bills_water.controller';
import { Bills_WaterService } from './bills_waterservice';
import { Bills_Water } from './entities/bills_water.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bills_Water])],
  controllers: [Bills_WaterController],
  providers: [Bills_WaterService],
})
export class Bills_WaterModule {}
