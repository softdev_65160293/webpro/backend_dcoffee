import { Test, TestingModule } from '@nestjs/testing';
import { Bills_WaterController } from './bills_water.controller';
import { Bills_WaterService } from './bills_waterservice';

describe('Bills_WaterController', () => {
  let controller: Bills_WaterController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Bills_WaterController],
      providers: [Bills_WaterService],
    }).compile();

    controller = module.get<Bills_WaterController>(Bills_WaterController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
